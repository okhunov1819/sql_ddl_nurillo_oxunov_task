CREATE TABLE nurillo_schema.dim_trainees (
    trainee_id         BigSERIAL PRIMARY KEY,
    group_id           BIGINT NOT NULL references nurillo_schema.dim_groups,
    first_name         TEXT NOT NULL,
    last_name          TEXT NOT NULL,
    full_name          TEXT  GENERATED ALWAYS AS (first_name || '' ||last_name) 
                       STORED NOT NULL,
    birth_date         DATE NOT NULL,
    enrollment_year    INTEGER DEFAULT EXTRACT (year FROM current_date) NOT NULL,
    graduation_year    INTEGER
);

INSERT INTO nurillo_schema.dim_trainees  (group_id,first_name, last_name, birth_date   )
VALUES (1, 'Nurillo', 'Oxunov', '2003-19-03');

SELECT * FROM nurillo_schema.dim_trainees;

ALTER TABLE nurillo_schema.dim_trainees ADD COLUMN 	education text;

ALTER TABLE nurillo_schema.dim_trainees
RENAME COLUMN graduation_year TO graduation_date; 

-- Allow null values temporarily
ALTER TABLE nurillo_schema.dim_trainees
ALTER COLUMN education DROP NOT NULL;

-- Update null values
UPDATE nurillo_schema.dim_trainees
SET education = 'Unknown'
WHERE education IS NULL;

-- Set the column as NOT NULL
ALTER TABLE nurillo_schema.dim_trainees
ALTER COLUMN education SET NOT NULL;


ALTER TABLE nurillo_schema.dim_trainees
ADD CONSTRAINT ck_birth_date CHECK (birth_date > '2003-19-03');

ALTER TABLE nurillo_schema.dim_trainees
ADD CONSTRAINT ck_non_negative_value CHECK (graduation_date >= 0);

ALTER TABLE nurillo_schema.dim_trainees
ADD COLUMN record_ts DATE DEFAULT current_date;

SELECT trainee_id, record_ts
FROM nurillo_schema.dim_trainees;





 