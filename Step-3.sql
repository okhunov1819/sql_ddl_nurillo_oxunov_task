CREATE TABLE nurillo_schema.dim_groups (
    group_id       BigSERIAL PRIMARY KEY,
    group_number   INTEGER NOT NULL,
    creation_year  INTEGER DEFAULT EXTRACT (year FROM current_date),
    disband_year   INTEGER
);

INSERT INTO nurillo_schema.dim_groups (group_number)
VALUES (101);

SELECT * FROM nurillo_schema.dim_groups;

ALTER TABLE nurillo_schemaa.dim_groups ADD COLUMN 	email text;

ALTER TABLE nurillo_schema.dim_groups 
RENAME COLUMN email  TO group_email;

ALTER TABLE nurillo_schema.dim_groups 
ALTER COLUMN group_email DROP NOT NULL;

ALTER TABLE nurillo_schema.dim_groups
ADD CONSTRAINT ck_creation_year CHECK (creation_year > '2003');

ALTER TABLE nurillo_schema.dim_groups
ADD CONSTRAINT ck_non_negative_group_number CHECK (group_number >= 0);

ALTER TABLE nurillo_schema.dim_groups
ADD CONSTRAINT ck_group_number_specific_value CHECK (group_number IN (101, 102, 103));

ALTER TABLE nurillo_schema.dim_groups
ADD CONSTRAINT uc_group_id UNIQUE (group_id);

ALTER TABLE nurillo_schema.dim_groups
ADD COLUMN record_ts DATE DEFAULT current_date;

SELECT group_id, record_ts
FROM nurillo_schema.dim_groups;