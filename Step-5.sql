CREATE TABLE nurillo_schema.contact_details 
(
    trainees_id    BigSERIAL PRIMARY KEY,
    address        VARCHAR(150),
    city           VARCHAR(50),
    state VARCHAR(50),
    country VARCHAR(50),
    postal_code VARCHAR(20),
    created_date TIMESTAMP DEFAULT current_timestamp
);

ALTER TABLE nurillo_schema.data.contact_details
ADD COLUMN record_ts DATE DEFAULT current_date;

SELECT contact_details.*, record_ts
FROM nurillo_schema.contact_details;


